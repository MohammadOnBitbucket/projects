.text
.macro nl()
li $v0, 4
la $a0, nl
syscall
.end_macro

.macro tab()
li $v0, 4
la $a0, tab
syscall
.end_macro

.macro space()
li $v0, 4
la $a0, space
syscall
.end_macro

#TESTING ARRAY FILL
li $a0, 0xffff0000
la $a1, fileName
li $a2, 4000

jal arrayFill

move $a0, $v0



li $v0, 1
syscall

nl()

#END TEST ARRAY FILL

#BEGIN TEST FIND2BYTE

li $a0, 0xffff0000
li $a1, 0x2F2B
li $a2, 25
li $a3, 80

jal find2Byte

move $a0, $v0
li $v0, 1
syscall

space()

move $a0, $v1
li $v0, 1
syscall

#END FIND2BYTE

#BEGIN PLAYGAME
li $a0, 0xffff0000
li $a1, 1
li $a2, 15
la $a3, testString1

jal playGame

li $a0, 0xffff0000
li $a1, -1
li $a2, -1
la $a3, testString

jal playGame



li $v0, 10
syscall

.data
testArray:	.space 4000
fileName: .asciiz "landscape2.map"
nl: .asciiz "\n"
tab: .asciiz "\t"
space: .asciiz "  "
testString1: "dd"
testString: "ss"
.include "hw3.asm"
