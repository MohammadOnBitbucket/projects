##############################################################
# Homework #3
# name: Mohammad Khan
# sbuid: 109958150
##############################################################
.text
.macro insert_plus()
	lb $t8, 0($s0) #load the first 2 hex values
	lb $t9, 1($s0) #load the next 2 hex values
	ori $t8, $t8, 0x2B #puts the plus sign on the tile
	ori $t9, $t9, 0x0F #makes the plus sign white
	sb $t8, 0($s0)
	sb $t9, 1($s0)
.end_macro

.macro remove_plus()
	lb $t8, 0($s0) #load the first 2 hex values
	lb $t9, 1($s0) #load the next 2 hex values
	andi $t8, $t8, 0x20 #remove the plus sign
	ori $t9, $t9, 0x80 #make the color lighter
	sb $t8, 0($s0)
	sb $t9, 1($s0)
.end_macro

.macro calc_addr()
	mul $s1, $t6, $t7 # # of cols * 2 in s1
	mul $s1, $t3, $s1 #i * cols * 2 in s1
	mul $s2, $t4, $t7 #j* 2 in s2
	add $s0, $s1, $s2 #(i*cols*2) + (j * 2) in s0
	add $s0, $t5, $s0 #base address + (i*cols*2) + (j * 2) in s0
.end_macro

.macro sleep()
	addi $sp, $sp, -8
	sw $a0, 0($sp)
	sw $v0, 4($sp)
	li $a0, 500
	li $v0, 32
	syscall
	lw $a0, 0($sp)
	lw $v0, 4($sp)
	addi $sp, $sp, 8
.end_macro

.macro check_collision()

.end_macro



##############################
# Part 1 FUNCTION
##############################

# @param arr Base address of the array in memory.
# @param filename Address of filename in memory.
# @param maxBytes Maximum number of bytes to read from the file into memory.
#@return int Return the number of bytes read from the file and stored in memory. -1 if an error occurs (ie. can't open file, etc)
arrayFill:
	#Define your code here
	addi $sp, $sp, -12 #saves argument registers onto stack
	sw $a0, 8($sp)
	sw $a1, 4($sp)
	sw $a2, 0($sp)
	move $a0, $a1 #sets up syscall 13
	li $a1, 0
	li $a2, 0
	li $v0, 13
	syscall
	
	bltz $v0, exitArrayFillLoopError
	
	move $t0, $v0
	
	move $a0, $v0 #sets up syscall 14
	lw $a1, 8($sp)
	li $a2, 1
	li $t1, 0
	lw $t2, 0($sp)
	
	ArrayFillLoop:
	li $v0, 14
	syscall
	beqz $v0, exitArrayFillLoop
	bltz $v0, exitArrayFillLoopError
	add $t1, $t1, $v0
	beq $t1, $t2, exitArrayFillLoop
	addi $a1, $a1, 1
	j ArrayFillLoop
	
	exitArrayFillLoop:
	addi $sp, $sp, 12
	li $v0, 16
	syscall
	move $v0, $t1
	jr $ra
	
	exitArrayFillLoopError:
	addi $sp, $sp, 12
	li $v0, 16
	syscall
	li $v0, -1
	jr $ra


##
# Calculate the row and column for the first occurrence of the
# 2-byte value in arr[row][column].
#
# @param arr Base address of the array in memory.
# @param value The 2-byte value to search for in the array.
# @param row The number of rows in the array.
# @param column The number of columns in the array.
#
# @return x The row in the array where the value was located.
# -1 if not found.
# @return y The column in the array where the value was located.
# -1 if not found.
#/		
find2Byte:
	#Define your code here
	move $t2, $a0 #moving array into t2
	li $t4, 0 #i
	li $t5, 0 #j
	li $t7, 2
	move $t0, $t2 #base address of the array
	loopFind2ByteThroughRow:
	lhu $t3, 0($t2)
	beq $t3, $a1, exitFind2Byte
	add $s3, $t4, $t5
	add $s4, $a2, $a3
	beq $s3 $s4, notFound
	mul $s1, $a3, $t7 #cols * 2 in s1
	mul $s1, $t4, $s1 #i * cols * 2 in s1
	mul $s2, $t5, $t7 #j* 2 in s2
	add $s0, $s1, $s2 #(i*cols*2) + (j * 2) in s0
	add $t2, $t0, $s0 #base address + (i*cols*2) + (j * 2) in s0
	beq $a3, $t5, addOneToRow
	addi $t5, $t5, 1
	j loopFind2ByteThroughRow
	
	addOneToRow:
	addi $t4, $t4, 1
	li $t5, 0
	j loopFind2ByteThroughRow
	
	notFound:
	li $v0, -1
	li $v1, -1
	jr $ra
	 
	exitFind2Byte:
	move $v0, $t4
	move $v1, $t5
	jr $ra

##############################
# PART 2/3 FUNCTION
##############################


# Play the lawn mower game. The mower is at position
# (start_r, start_c). Move the lawn mower according to
# each character in moves string.
# @param A Base address of the memory array.
# @param start_r The row of the mower.
# @param start_c The column of the mower.
# @param moves Address of string of characters for moves.


playGame:
	#Define your code here
	move $t3, $a1 #i row of the mower
	move $t4, $a2 #j column of the mower
	move $t5, $a0 #base address
	li $t6, 80
	li $t7, 2
	li $s5, 45167
	beq $a1, -1, negRow
	
	setMowerPosition:
	calc_addr()
	sleep()
	insert_plus()
	
	loopReadString:
	lb $t0, 0($a3)
	beqz $t0, exitAndSavePos
	beq $t0, 119, moveUp
	beq $t0, 97, moveLeft
	beq $t0, 100, moveRight
	beq $t0, 115, moveDown
	addi $a3, $a3, 1
	j loopReadString
	
	negRow:
	beq $a2,-1, setToPrev
	
	setToPrev:
	lw $t3, mower_prev_row
	lw $t4, mower_prev_col
	j setMowerPosition
	
	#move by using base address i + (i*#cols # 2) + (2*j) where i and j are the desired row and column so for example 
	#if you are at 27 49 and you want to move up add 1 to your row register and then compute the address with the formula
	#later add a branch checking for all the tiles that you CANNOT go through
	
	#to check for what kind of tile the next one is first find the address of the 
	
	moveUp:
	calc_addr()
	sleep()
	remove_plus()
	sleep() 
	addi $t3, $t3, -1
	beq $t3, -1, warpDown
	wD:
	calc_addr()
	lhu $s3, 0($s0)
	beq $s3, 32594, collisionUp #rocks
	beq $s3, 20256, collisionUp #water
	beq $s3, 12126, collisionUp #trees
	beq $s3, 16160, collisionUp #dirt
	beq $s3, $s5, collisionUp #flowers
	insert_plus()
	sleep()
	addi $a3, $a3, 1
	j loopReadString
	
	moveLeft:
	calc_addr()
	sleep()
	remove_plus()
	sleep()
	addi $t4, $t4, -1
	beq $t4, -1, warpRight
	wR:
	calc_addr()
	lhu $s3, 0($s0)
	beq $s3, 32594, collisionLeft #rocks
	beq $s3, 20256, collisionLeft #water
	beq $s3, 12126, collisionLeft #trees
	beq $s3, 16160, collisionLeft #dirt
	beq $s3, $s5, collisionLeft #flowers
	insert_plus()
	sleep()
	addi $a3, $a3, 1
	j loopReadString
	
	moveRight:
	calc_addr()
	sleep()
	remove_plus()
	sleep()
	addi $t4, $t4, 1
	beq $t4, 81, warpLeft
	wL:
	calc_addr()
	lhu $s3, 0($s0)
	beq $s3, 32594, collisionRight #rocks
	beq $s3, 20256, collisionRight #water
	beq $s3, 12126, collisionRight #trees
	beq $s3, 16160, collisionRight #dirt
	beq $s3, $s5 , collisionRight #flowers
	insert_plus()
	sleep()
	addi $a3, $a3, 1
	j loopReadString
	
	moveDown:
	calc_addr()
	sleep()
	remove_plus()
	sleep()
	addi $t3, $t3, 1
	beq $t3, 25, warpTop
	wT:
	calc_addr()
	lhu $s3, 0($s0)
	beq $s3, 32594, collisionDown #rocks
	beq $s3, 20256, collisionDown #water
	beq $s3, 12126, collisionDown #trees
	beq $s3, 16160, collisionDown #dirt
	beq $s3, $s5, collisionDown #flowers
	insert_plus()
	sleep()
	addi $a3, $a3, 1
	j loopReadString
	
	collisionUp:
	addi $t3, $t3, 1
	addi $a3, $a3, 1
	lb $s7, 0($a3)
	beqz $s7, putPlusStuck
	j loopReadString
	
	collisionLeft:
	addi $t4, $t4, 1
	addi $a3, $a3, 1
	lb $s7, 0($a3)
	beqz $s7, putPlusStuck
	j loopReadString
	
	collisionRight:
	addi $t4, $t4, -1
	addi $a3, $a3, 1
	lb $s7, 0($a3)
	beqz $s7, putPlusStuck
	j loopReadString
	
	collisionDown:
	addi $t3, $t3, 1
	addi $a3, $a3, 1
	lb $s7, 0($a3)
	beqz $s7, putPlusStuck
	j loopReadString
	
	warpDown:
	li $t3, 24
	j wD
	
	warpTop:
	li $t3, 0
	j wT
	
	warpLeft:
	li $t4, 0
	j wL
	
	warpRight:
	li $t4, 79
	j wR
	
	putPlusStuck:
	calc_addr()
	insert_plus()
	
	exitAndSavePos:
	move $a1, $t3
	move $a2, $t4
	sw $a1, mower_prev_row
	sw $a2, mower_prev_col
	jr $ra


.data
	#Define your memory here 
	mower_prev_row: .word 24
	mower_prev_col: .word 79
	flower: .word 45167
	
