# Homework #1
# name: Mohammad Khan
# sbuid: 109958150

.text
	.macro nl()
	la $a0, nl
	li $v0, 4
	syscall
	.end_macro
	
	.macro tab()
	la $a0, tab
	li $v0, 4
	syscall
	.end_macro
	
.globl main
main:
	#part 1 start
	
	la $a0, prompt
	li $v0, 4
	syscall #print out the prompt

	li $v0, 5
	syscall #get the user input
	
	move $s0, $v0 #save the user input in a saved register
	
	#part 1 end
			
	nl()
	
	#part 2 start
	
	la $a0, twocomp # start 2's comp
	li $v0, 4
	syscall
	
	tab()
	tab()
	
	move $a0, $s0
	li $v0, 1
	syscall #rep value
	
	tab()
	
	move $a0, $s0
	li $v0, 34
	syscall #hex value

	tab()
	
	move $a0, $s0
	li $v0, 35 
	syscall #binary value
	
	tab()
	
	move $a0, $s0
	li $v0, 1
	syscall #rep value end 2's comp
	
	nl()
	
	la $a0, onecomp #start 1's comp 
	li $v0, 4
	syscall
	
	tab()
	tab()
	
	move $t0, $s0
	bgez $t0, addneg1
	addiu $t0, $t0, -1 #subtract the extra 1 to make it the right rep value if value less than 0
	addneg1: #skip if greater than equal zero
	move $a0, $t0
	li $v0, 100
	syscall	#rep value
	
	tab()
	
	move $a0, $t0
	li $v0, 34
	syscall #hex value
	
	tab()
	
	move $a0, $t0
	li $v0, 35
	syscall #binary value
	
	tab()
	
	move $a0, $t0
	li $v0, 100
	syscall #rep value end 1's comp
	
	nl()
	
	la $a0, negonecomp #start neg 1's comp
	li $v0, 4
	syscall 
	
	tab()
		
	not $t1, $t0
	move $a0, $t1
	li $v0, 100
	syscall #rep value
	
	tab()
	
	move $a0, $t1
	li $v0, 34
	syscall #hex value
	
	tab()
	
	move $a0, $t1
	li $v0, 35
	syscall #binary value
	
	tab()
	
	move $a0, $t1
	li $v0, 100
	syscall #rep value
	
	move $t1, $s0	
	nl()
	
	la $a0, negsm #start negsm
	li $v0, 4
	syscall
	
	bltz $s0, negsmltz
	
	li $t2, 2147483648
	or $t1, $t1, $t2
	
	j ayylmao
	
	negsmltz:
	not $t1, $s0 
	addiu $t1, $t1, 1

	ayylmao:
	
	tab()
	
	move $a0, $t1
	li $v0, 101
	syscall #rep value
	
	tab()
	
	move $a0, $t1
	li $v0, 34
	syscall #hex value
	
	tab()
	
	move $a0, $t1
	li $v0, 35
	syscall #binary value
	
	tab()
	
	move $a0, $t1
	li $v0, 101
	syscall #rep value end negsm
	
	#part 2 end
	
	nl()
	nl()
	
	#part 3 start
	
	la $a0, sp #print sp string
	li $v0, 4
	syscall
	
	move $t1, $s0
	
	
	andi $t3, $t1, 2147483648 #signed bit isolation
	srl $t3, $t3, 31 #signed bit isolation
	
	andi $t4, $t1, 8388607 #mantissa isolation
	sll $t4, $t4, 9 #mantissa isolation
	srl $t4, $t4, 9 #mantissa isolation
	
	andi $t2, $t1, 2139095040 #exponent isolation
	srl $t2, $t2, 23 #exponent isolation
	
	move $a0, $t2
	li $v0, 1
	syscall
	
	tab()
	
	addiu $t5, $t2, -127
	move $a0, $t5
	li $v0, 1
	syscall
	
	tab() 
	
	beqz $t2, checkmantzero
	
	j checkexpone
	
	checkexpone:
	beq $t2, 255, checkmantzeroinf
	
	j derp
	
	checkmantzeroinf:
	beqz $t4, checksignbitinf
	
	j checkmantone
	
	checkmantone:
	li $t7, 8388607
	beq $t4, $t7, printnan
	
	j derp

	printnan:
	la $a0, shankyournan
	li $v0, 4
	syscall
	
	j derp
							
	checksignbitinf:
	beqz $t3, printpinf

	j printninf
	
	printpinf:
	la $a0, pinf
	li $v0, 4
	syscall
	
	j derp
	
	printninf:
	la $a0, ninf
	li $v0, 4
	syscall	
	
	j derp
	
	checkmantzero:
	beqz $t4, checksignbit
	
	j derp
	
	checksignbit:
	beqz $t3, printposzero
	
	j printnegzero
	
	printposzero: 
	la $a0, posz
	li $v0, 4
	syscall
	
	j derp
	
	printnegzero:
	la $a0, negz
	li $v0, 4
	syscall
	
	derp: 
	
	nl()	
	
	la $a0, dp
	li $v0, 4
	syscall
	
	move $t1, $s0
	
	andi $t2, $t1, 2146435072 #exponent isolation
	srl $t2, $t2, 20 #exponent isolation 
	
	andi $t3, $t1, 2147483648 #signed bit isolation
	srl $t3, $t3, 31 #signed bit isolation
	
	andi $t4, $t1, 1048575 #mantissa isolation
	sll $t4, $t4, 12 #mantissa isolation
	srl $t4, $t4, 12 #mantissa isolation
	
	move $a0, $t2
	li $v0, 1
	syscall
	
	tab()
	
	addiu $t5, $t2, -1023
	move $a0, $t5
	li $v0, 1
	syscall # part 3 end
	
	tab() 
	
	beqz $t2, checkmantzerod
	
	j checkexponed
	
	checkexponed:
	li $t6, 2146435072
	beq $t2, $t7, checkmantzeroinfd
	
	j derpd
	
	checkmantzeroinfd:
	beqz $t4, checksignbitinfd
	
	j checkmantoned
	
	checkmantoned:
	li $t7, 1048575
	beq $t4, $t7, printnand
	
	j derpd

	printnand:
	la $a0, shankyournan
	li $v0, 4
	syscall
	
	j derpd
							
	checksignbitinfd:
	beqz $t3, printpinfd

	j printninfd
	
	printpinfd:

	li $v0, 4
	syscall
	
	j derpd
	
	printninfd:
	la $a0, ninf
	li $v0, 4
	syscall	
	
	j derpd
	
	checkmantzerod:
	beqz $t4, checksignbitd
	
	j derpd
	
	checksignbitd:
	beqz $t3, printposzerod
	
	j printnegzerod
	
	printposzerod: 
	la $a0, posz
	li $v0, 4
	syscall
	
	j derp
	
	printnegzerod:
	la $a0, negz
	li $v0, 4
	syscall
	
	derpd:
	
	nl()
	nl()
	
	#part 4
	
	
	la $a0, promptc
	li $v0, 4
	syscall
	
	li $v0, 12
	syscall	
	move $t1, $v0
	
	nl()
	
	la $a0, promptc
	li $v0, 4
	syscall
	
	li $v0, 12
	syscall
	move $t2, $v0
	
	nl()
	
	la $a0, promptc
	li $v0, 4
	syscall
	
	li $v0, 12
	syscall
	move $t3, $v0 	
	
	nl()
	
	la $a0, promptc
	li $v0, 4
	syscall
	
	li $v0, 12
	syscall	
	move $t4, $v0
	
	sll $t4, $t4, 24
	sll $t3, $t3, 16
	sll $t2, $t2, 8
	
	or $t5, $t4, $t3
	or $t5, $t5, $t2
	or $t5, $t5, $t1
	
	move $s1, $t5
	
	nl()
	nl()
	
	la $a0, twocomp # start 2's comp
	li $v0, 4
	syscall
	
	tab()
	tab()
	
	move $a0, $s1
	li $v0, 1
	syscall #rep value
	
	tab()
	
	move $a0, $s1
	li $v0, 34
	syscall #hex value

	tab()
	
	move $a0, $s1
	li $v0, 35 
	syscall #binary value
	
	tab()
	
	move $a0, $s1
	li $v0, 1
	syscall #rep value end 2's comp
	
	nl()
	
	la $a0, onecomp #start 1's comp 
	li $v0, 4
	syscall
	
	tab()
	tab()
	
	move $t0, $s1
	bgez $t0, addneg1a
	addiu $t0, $t0, -1 #subtract the extra 1 to make it the right rep value if value less than 0
	addneg1a: #skip if greater than equal zero
	move $a0, $t0
	li $v0, 100
	syscall	#rep value
	
	tab()
	
	move $a0, $t0
	li $v0, 34
	syscall #hex value
	
	tab()
	
	move $a0, $t0
	li $v0, 35
	syscall #binary value
	
	tab()
	
	move $a0, $t0
	li $v0, 100
	syscall #rep value end 1's comp
	
	nl()
	
	la $a0, negonecomp #start neg 1's comp
	li $v0, 4
	syscall 
	
	tab()
	not $t1, $t0
	move $a0, $t1
	li $v0, 100
	syscall #rep value
	
	tab()
	
	move $a0, $t1
	li $v0, 34
	syscall #hex value
	
	tab()
	
	move $a0, $t1
	li $v0, 35
	syscall #binary value
	
	tab()
	
	move $a0, $t1
	li $v0, 100
	syscall #rep value
	
	move $t1, $s1	
	nl()
	
	la $a0, negsm #start negsm
	li $v0, 4
	syscall
	
	bltz $s1, negsmltza
	
	li $t2, 2147483648
	or $t1, $t1, $t2
	
	j ayylmaoa
	
	negsmltza:
	not $t1, $s1 
	addiu $t1, $t1, 1

	ayylmaoa:
	
	tab()
	
	move $a0, $t1
	li $v0, 101
	syscall #rep value
	
	tab()
	
	move $a0, $t1
	li $v0, 34
	syscall #hex value
	
	tab()
	
	move $a0, $t1
	li $v0, 35
	syscall #binary value
	
	tab()
	
	move $a0, $t1
	li $v0, 101
	syscall #rep value end negsm
	
	li $v0, 10
	syscall
	
	
.data
prompt: .asciiz "Enter an integer number: "
twocomp: .asciiz "2's complement: "
onecomp: .asciiz "1's complement: "
negonecomp: .asciiz "Neg 1's complement:"
negsm: .asciiz "Neg Signed Magnitude: "
tab: .asciiz "\t"
nl: .asciiz "\n"
sp: .asciiz "IEEE-754 single precision: "
dp: .asciiz "IEEE-754 double precision: "
pinf: .asciiz "+INF"
ninf: .asciiz "-INF"
shankyournan: .asciiz "NaN"
posz: .asciiz "+0"
negz: .asciiz "-0"
promptc: .asciiz "Enter a character: "
