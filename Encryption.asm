
##############################################################
# Homework #2
# name: Mohammad Khan
# sbuid: 109958150
##############################################################
.text

##############################
# EASY HELPER FUNCTIONS 
# Suggestion: Do these first
##############################


toUpper:
        ble $a0, 96, exit
        bge $a0, 123, exit 
        move $t0, $a0
   
        addi $v0, $t0, -32 
        jr $ra
        exit:
             move $v0, $a0
	     jr $ra

countChars:
	li $v0, 0
	move $t2, $a0
	count:
	     lb $t1, 0($t2)
	     addi $t2, $t2, 1
	     beqz $t1, done
	     ble $t1, 64, count
	     bge $t1, 123, count
	     beq $t1, 91, count
	     beq $t1, 92, count
	     beq $t1, 93, count
	     beq $t1, 94, count
	     beq $t1, 95, count
	     beq $t1, 96, count
	     addi $v0, $v0, 1
	     j count
	done:      
	     jr $ra

toLower:
       move $t2, $a0       
       loop:
            lb $t0, 0($t2)
            addi $t2, $t2, 1
            beqz $t0, exit2
            ble $t0, 64, loop
            bge $t0, 91, loop
            addi $t0, $t0, 32
            sb $t0, -1($t2) 
            j loop
                    
       exit2:
            move $v0, $a0
            jr $ra       
        

##############################
# PART 1 FUNCTION
##############################

decodeNull:
	move $t2, $a0
	move $t1, $a3
	li $t3, 1
	loopd:	 
	lb $t0, 0($t2)
	lw $t4, 0($t1)
	beqz $t4, nextWord
	ble $t0, 64, almao
	bge $t0, 123, almao
	beq $t1, 91, almao
	beq $t1, 92, almao
	beq $t1, 93, almao
	beq $t1, 94, almao
	beq $t1, 95, almao
	beq $t1, 96, almao
	beq $t3, $t4, increment
	addi $t3, $t3, 1
	beq $t4, -1, exitdcn
	addi $t2, $t2, 1
	j loopd
	
	increment: 
	addi $sp, $sp, -8
	sw $a0, 0($sp)
	sw $ra, 4($sp)
	move $a0, $t0 
	jal toUpper
	lw $a0, 0($sp)
	lw $ra, 4($sp)
	addi $sp, $sp, 8
	sb $v0, 0($a1)
	addi $a1, $a1, 1
	j nextWord
	
	nextWord:
	lb $t5, 0($t2)
	addi $t2, $t2, 1
	beq $t5, 32, reset
	j nextWord
	
	reset:
	li $t3, 1
	addi $t1, $t1, 4
	j loopd
	
	almao:
	addi $t2, $t2, 1
	j loopd
	
	
	exitdcn:
	jr $ra

##############################
# PART 2 FUNCTIONS
##############################

genBacon:
	addi $sp, $sp, -20
	sw $a0, 0($sp)
	sw $a1, 4($sp)
	sw $a2, 8($sp)
	sw $a3, 12($sp)
	sw $ra, 16($sp)
	jal toLower
	lw $a0, 0($sp)
	lw $a1, 4($sp)
	lw $a2, 8($sp)
	lw $a3, 12($sp)
	lw $ra, 16($sp)	
	addi $sp, $sp, 20
	
	move $t3, $a1
	
	loopgenBacon:
	lb $t0, 0($a0)
	addi $a0, $a0, 1
	beq $t0, 97, a
	beq $t0, 98, bB
	beq $t0, 99, c
	beq $t0, 100, c
	beq $t0, 101, d
	beq $t0, 102, e
	beq $t0, 103, f
	beq $t0, 104, g
	beq $t0, 105, h
	beq $t0, 106, i
	beq $t0, 107, jJ
	beq $t0, 108, k
	beq $t0, 109, l
	beq $t0, 110, m
	beq $t0, 111, n
	beq $t0, 112, o
	beq $t0, 113, p
	beq $t0, 114, q
	beq $t0, 115, r
	beq $t0, 116, s
	beq $t0, 117, t
	beq $t0, 118, u
	beq $t0, 119, v
	beq $t0, 120, x
	beq $t0, 121, y
	beq $t0, 122, z
	beq $t0, 32, space
	beq $t0, 33, exclamation
	beq $t0, 39, apostrophe
	beq $t0, 44, comma
	beq $t0, 46, period
	beqz $t0, eom
	j loopgenBacon

	
	a:
	sb $a2, 0($a1)
	addi $a1, $a1, 1
	sb $a2, 0($a1)
	addi $a1, $a1, 1
	sb $a2, 0($a1)
	addi $a1, $a1, 1
	sb $a2, 0($a1)
	addi $a1, $a1, 1
	sb $a2, 0($a1)
	addi $a1, $a1, 1
	j loopgenBacon
	
	bB:
	sb $a2, 0($a1)
	addi $a1, $a1, 1
	sb $a2, 0($a1)
	addi $a1, $a1, 1
	sb $a2, 0($a1)
	addi $a1, $a1, 1
	sb $a2, 0($a1)
	addi $a1, $a1, 1
	sb $a3, 0($a1)
	addi $a1, $a1, 1
	j loopgenBacon
	
	c:
	sb $a2, 0($a1)
	addi $a1, $a1, 1
	sb $a2, 0($a1)
	addi $a1, $a1, 1
	sb $a2, 0($a1)
	addi $a1, $a1, 1
	sb $a3, 0($a1)
	addi $a1, $a1, 1
	sb $a2, 0($a1)
	addi $a1, $a1, 1
	j loopgenBacon  
	
	d:
	sb $a2, 0($a1)
	addi $a1, $a1, 1
	sb $a2, 0($a1)
	addi $a1, $a1, 1
	sb $a2, 0($a1)
	addi $a1, $a1, 1
	sb $a3, 0($a1)
	addi $a1, $a1, 1
	sb $a3, 0($a1)
	addi $a1, $a1, 1
	j loopgenBacon
	
	e:
	sb $a2, 0($a1)
	addi $a1, $a1, 1
	sb $a2, 0($a1)
	addi $a1, $a1, 1
	sb $a3, 0($a1)
	addi $a1, $a1, 1
	sb $a2, 0($a1)
	addi $a1, $a1, 1
	sb $a2, 0($a1)
	addi $a1, $a1, 1
	j loopgenBacon
	jr $ra
	
	f:
	sb $a2, 0($a1)
	addi $a1, $a1, 1
	sb $a2, 0($a1)
	addi $a1, $a1, 1
	sb $a3, 0($a1)
	addi $a1, $a1, 1
	sb $a2, 0($a1)
	addi $a1, $a1, 1
	sb $a3, 0($a1)
	addi $a1, $a1, 1
	j loopgenBacon

	g:
	sb $a2, 0($a1)
	addi $a1, $a1, 1
	sb $a2, 0($a1)
	addi $a1, $a1, 1
	sb $a3, 0($a1)
	addi $a1, $a1, 1
	sb $a3, 0($a1)
	addi $a1, $a1, 1
	sb $a2, 0($a1)
	addi $a1, $a1, 1
	j loopgenBacon
	
	h:
	sb $a2, 0($a1)
	addi $a1, $a1, 1
	sb $a3, 0($a1)
	addi $a1, $a1, 1
	sb $a2, 0($a1)
	addi $a1, $a1, 1
	sb $a2, 0($a1)
	addi $a1, $a1, 1
	sb $a3, 0($a1)
	addi $a1, $a1, 1
	j loopgenBacon
	
	i:
	sb $a2, 0($a1)
	addi $a1, $a1, 1
	sb $a2, 0($a1)
	addi $a1, $a1, 1
	sb $a2, 0($a1)
	addi $a1, $a1, 1
	sb $a2, 0($a1)
	addi $a1, $a1, 1
	sb $a3, 0($a1)
	addi $a1, $a1, 1
	j loopgenBacon
	
	jJ:
	sb $a2, 0($a1)
	addi $a1, $a1, 1
	sb $a3, 0($a1)
	addi $a1, $a1, 1
	sb $a2, 0($a1)
	addi $a1, $a1, 1
	sb $a2, 0($a1)
	addi $a1, $a1, 1
	sb $a3, 0($a1)
	addi $a1, $a1, 1
	j loopgenBacon
	
	k:
	sb $a2, 0($a1)
	addi $a1, $a1, 1
	sb $a3, 0($a1)
	addi $a1, $a1, 1
	sb $a2, 0($a1)
	addi $a1, $a1, 1
	sb $a3, 0($a1)
	addi $a1, $a1, 1
	sb $a2, 0($a1)
	addi $a1, $a1, 1
	j loopgenBacon
	
	l:
	sb $a2, 0($a1)
	addi $a1, $a1, 1
	sb $a3, 0($a1)
	addi $a1, $a1, 1
	sb $a2, 0($a1)
	addi $a1, $a1, 1
	sb $a3, 0($a1)
	addi $a1, $a1, 1
	sb $a3, 0($a1)
	addi $a1, $a1, 1
	j loopgenBacon
	
	m:
	sb $a2, 0($a1)
	addi $a1, $a1, 1
	sb $a3, 0($a1)
	addi $a1, $a1, 1
	sb $a3, 0($a1)
	addi $a1, $a1, 1
	sb $a2, 0($a1)
	addi $a1, $a1, 1
	sb $a2, 0($a1)
	addi $a1, $a1, 1
	j loopgenBacon
	
	n:
	sb $a2, 0($a1)
	addi $a1, $a1, 1
	sb $a3, 0($a1)
	addi $a1, $a1, 1
	sb $a3, 0($a1)
	addi $a1, $a1, 1
	sb $a2, 0($a1)
	addi $a1, $a1, 1
	sb $a3, 0($a1)
	addi $a1, $a1, 1
	j loopgenBacon
	
	o:
	sb $a2, 0($a1)
	addi $a1, $a1, 1
	sb $a3, 0($a1)
	addi $a1, $a1, 1
	sb $a3, 0($a1)
	addi $a1, $a1, 1
	sb $a3, 0($a1)
	addi $a1, $a1, 1
	sb $a2, 0($a1)
	addi $a1, $a1, 1
	j loopgenBacon
	
	p:
	sb $a2, 0($a1)
	addi $a1, $a1, 1
	sb $a3, 0($a1)
	addi $a1, $a1, 1
	sb $a3, 0($a1)
	addi $a1, $a1, 1
	sb $a3, 0($a1)
	addi $a1, $a1, 1
	sb $a3, 0($a1)
	addi $a1, $a1, 1
	j loopgenBacon
	
	q:
	sb $a2, 0($a1)
	addi $a1, $a1, 1
	sb $a3, 0($a1)
	addi $a1, $a1, 1
	sb $a3, 0($a1)
	addi $a1, $a1, 1
	sb $a3, 0($a1)
	addi $a1, $a1, 1
	sb $a3, 0($a1)
	addi $a1, $a1, 1
	j loopgenBacon
	
	r:
	sb $a3, 0($a1)
	addi $a1, $a1, 1
	sb $a2, 0($a1)
	addi $a1, $a1, 1
	sb $a2, 0($a1)
	addi $a1, $a1, 1
	sb $a2, 0($a1)
	addi $a1, $a1, 1
	sb $a3, 0($a1)
	addi $a1, $a1, 1
	j loopgenBacon
	
	s:
	sb $a3, 0($a1)
	addi $a1, $a1, 1
	sb $a2, 0($a1)
	addi $a1, $a1, 1
	sb $a2, 0($a1)
	addi $a1, $a1, 1
	sb $a3, 0($a1)
	addi $a1, $a1, 1
	sb $a2, 0($a1)
	addi $a1, $a1, 1
	j loopgenBacon
	
	t:
	sb $a3, 0($a1)
	addi $a1, $a1, 1
	sb $a2, 0($a1)
	addi $a1, $a1, 1
	sb $a2, 0($a1)
	addi $a1, $a1, 1
	sb $a3, 0($a1)
	addi $a1, $a1, 1
	sb $a3, 0($a1)
	addi $a1, $a1, 1
	j loopgenBacon
	
	u:
	sb $a2, 0($a1)
	addi $a1, $a1, 1
	sb $a2, 0($a1)
	addi $a1, $a1, 1
	sb $a2, 0($a1)
	addi $a1, $a1, 1
	sb $a2, 0($a1)
	addi $a1, $a1, 1
	sb $a3, 0($a1)
	addi $a1, $a1, 1
	j loopgenBacon
	
	v:
	sb $a3, 0($a1)
	addi $a1, $a1, 1
	sb $a2, 0($a1)
	addi $a1, $a1, 1
	sb $a3, 0($a1)
	addi $a1, $a1, 1
	sb $a2, 0($a1)
	addi $a1, $a1, 1
	sb $a3, 0($a1)
	addi $a1, $a1, 1
	j loopgenBacon
	
	w:
	sb $a3, 0($a1)
	addi $a1, $a1, 1
	sb $a2, 0($a1)
	addi $a1, $a1, 1
	sb $a3, 0($a1)
	addi $a1, $a1, 1
	sb $a3, 0($a1)
	addi $a1, $a1, 1
	sb $a2, 0($a1)
	addi $a1, $a1, 1
	j loopgenBacon
	
	x:
	sb $a3, 0($a1)
	addi $a1, $a1, 1
	sb $a2, 0($a1)
	addi $a1, $a1, 1
	sb $a3, 0($a1)
	addi $a1, $a1, 1
	sb $a3, 0($a1)
	addi $a1, $a1, 1
	sb $a3, 0($a1)
	addi $a1, $a1, 1
	j loopgenBacon
	
	y:
	sb $a3, 0($a1)
	addi $a1, $a1, 1
	sb $a3, 0($a1)
	addi $a1, $a1, 1
	sb $a2, 0($a1)
	addi $a1, $a1, 1
	sb $a2, 0($a1)
	addi $a1, $a1, 1
	sb $a2, 0($a1)
	addi $a1, $a1, 1
	j loopgenBacon
	
	z:
	sb $a3, 0($a1)
	addi $a1, $a1, 1
	sb $a3, 0($a1)
	addi $a1, $a1, 1
	sb $a2, 0($a1)
	addi $a1, $a1, 1
	sb $a2, 0($a1)
	addi $a1, $a1, 1
	sb $a3, 0($a1)
	addi $a1, $a1, 1
	j loopgenBacon
	
	space:
	sb $a3, 0($a1)
	addi $a1, $a1, 1
	sb $a3, 0($a1)
	addi $a1, $a1, 1
	sb $a2, 0($a1)
	addi $a1, $a1, 1
	sb $a3, 0($a1)
	addi $a1, $a1, 1
	sb $a2, 0($a1)
	addi $a1, $a1, 1
	j loopgenBacon
	
	exclamation:
	sb $a3, 0($a1)
	addi $a1, $a1, 1
	sb $a3, 0($a1)
	addi $a1, $a1, 1
	sb $a2, 0($a1)
	addi $a1, $a1, 1
	sb $a3, 0($a1)
	addi $a1, $a1, 1
	sb $a3, 0($a1)
	addi $a1, $a1, 1
	j loopgenBacon
	
	apostrophe:
	sb $a3, 0($a1)
	addi $a1, $a1, 1
	sb $a3, 0($a1)
	addi $a1, $a1, 1
	sb $a3, 0($a1)
	addi $a1, $a1, 1
	sb $a2, 0($a1)
	addi $a1, $a1, 1
	sb $a2, 0($a1)
	addi $a1, $a1, 1
	j loopgenBacon
	
	comma:
	sb $a3, 0($a1)
	addi $a1, $a1, 1
	sb $a3, 0($a1)
	addi $a3, $a1, 1
	sb $a2, 0($a1)
	addi $a1, $a1, 1
	sb $a2, 0($a1)
	addi $a1, $a1, 1
	sb $a2, 0($a1)
	addi $a1, $a1, 1
	j loopgenBacon
	
	period:
	sb $a3, 0($a1)
	addi $a1, $a1, 1
	sb $a3, 0($a1)
	addi $a1, $a1, 1
	sb $a3, 0($a1)
	addi $a1, $a1, 1
	sb $a3, 0($a1)
	addi $a1, $a1, 1
	sb $a2, 0($a1)
	addi $a1, $a1, 1
	j loopgenBacon
	
	eom:
	sb $a3, 0($a1)
	addi $a1, $a1, 1
	sb $a3, 0($a1)
	addi $a1, $a1, 1
	sb $a3, 0($a1)
	addi $a1, $a1, 1
	sb $a3, 0($a1)
	addi $a1, $a1, 1
	sb $a3, 0($a1)
	addi $a1, $a1, 1
	j exitgenBacon
	
	exitgenBacon:
	addi $sp, $sp, -24
	sw $a0, 0($sp)
	sw $a1, 4($sp)
	sw $a2, 8($sp)
	sw $a3, 12($sp)
	sw $ra, 16($sp)
	sw $t3, 20($sp)
	move $a0, $t3
	jal countChars
	lw $a0, 0($sp)
	lw $a1, 4($sp)
	lw $a2, 8($sp)
	lw $a3, 12($sp)
	lw $ra, 16($sp)
	lw $t3, 20($sp)	
	addi $sp, $sp, 24
	move $v1, $v0
	move $v0, $t3
	jr $ra
	
hideEncoding:
	addi $sp, $sp, -20
	sw $a0, 0($sp)
	sw $a1, 4($sp)
	sw $a2, 8($sp)
	sw $a3, 12($sp)
	sw $ra, 16($sp)
	move $a0, $a1
	jal toLower
	lw $a0, 0($sp)
	lw $a1, 4($sp)
	lw $a2, 8($sp)
	lw $a3, 12($sp)
	lw $ra, 16($sp)
	
	li $t4, 0
	
	loophideEncoding:
	lb $t1, 0($a0) #bacon byte
	lb $t2, 0($a1) #string byte
	ble $t2, 96, incrementhideEncoding
	bge $t2, 123, incrementhideEncoding
	beq $t1, $a2, makeCharUpper
	beqz $t2, exithideEncodingString 
	beqz $t1, exithideEncodingBacon
	addi $a0, $a0, 1
	addi $a1, $a1, 1
	addi $t4, $t4, 1
	j loophideEncoding
	
	makeCharUpper:
	addi $sp, $sp, -32
	sw $a0, 0($sp)
	sw $a1, 4($sp)
	sw $a2, 8($sp)
	sw $a3, 12($sp)
	sw $ra, 16($sp)
	sw $t1, 20($sp)
	sw $t2, 24($sp)
	sw $t4, 28($sp)
	move $a0, $t2
	jal toUpper
	lw $a0, 0($sp)
	lw $a1, 4($sp)
	lw $a2, 8($sp)
	lw $a3, 12($sp)
	lw $ra, 16($sp)
	lw $t1, 20($sp)
	lw $t2, 24($sp)
	lw $t4, 28($sp)
	addi $sp, $sp, 32
	sb $v0, 0($a1)
	addi $a0, $a0, 1
	addi $a1, $a1, 1
	addi $t4, $t4, 1
	j loophideEncoding
	
	incrementhideEncoding:
	addi $a1, $a1, 1
	j loophideEncoding
	
	exithideEncodingBacon:
	move $v0, $t4
	li $v1, 1
	jr $ra
	
	exithideEncodingString:
	move $v0, $t4
	li $v1, 0
	jr $ra

findEncoding:
	
	beq $a2, 65, asyml
	beq $a2, 90, zsyml
	beq $a2, 97, asymll
	beq $a2, 122, zsymll
	
	addi $t0, $a2, 1
	
	move $t2, $a0	
	
	li $t4, 0
	
	loopfindEncoding:
	lb $t1, 0($a1)
	addi $a1, $a1, 1
	bge $t1, 65, checkUpper
	bge $t1, 97, checkLower
	beq $t4, $a3, exitFindEncoding
	beqz $t1, exitFindEncoding
	j loopfindEncoding
	
	asyml:
	addi $t0, $a2, 1
	j loopfindEncoding
	
	zsyml:
	addi $t0, $a2, -1
	j loopfindEncoding
	
	asymll:
	addi $t0, $a2, 1
	j loopfindEncoding
	
	zsymll:
	addi $t0, $a2, -1
	j loopfindEncoding
	
	checkUpper:
	ble $t1, 90, addB
	j addOther
	
	checkLower:
	ble $t1, 122, addOther
	j loopfindEncoding
	
	addB:
	sb $a2, 0($a0)
	addi $a0, $a0, 1
	addi $t4, $t4, 1
	j loopfindEncoding 
	
	addOther:
	sb $t0, 0($a0)
	addi $a0, $a0, 1
	addi $t4, $t4, 1
	j loopfindEncoding
	
	true:
	li $v1, 1
	j exitfe
	
	exitFindEncoding:
	addi $sp, $sp, -44
	sw $a0, 0($sp)
	sw $a1, 4($sp)
	sw $a2, 8($sp)
	sw $a3, 12($sp)
	sw $ra, 16($sp)
	sw $t1, 20($sp)
	sw $t2, 24($sp)
	sw $t3, 28($sp)
	sw $t4, 32($sp)
	sw $t6, 36($sp)
	sw $t0, 40($sp)
	move $a0, $t2
	jal countChars
	lw $a0, 0($sp)
	lw $a1, 4($sp)
	lw $a2, 8($sp)
	lw $a3, 12($sp)
	lw $ra, 16($sp)
	sw $t1, 20($sp)
	sw $t2, 24($sp)
	sw $t3, 28($sp)
	sw $t4, 32($sp)
	sw $t6, 36($sp)
	sw $t0, 40($sp)
	addi $sp, $sp, 44	
	sb $zero, 0($a0)
	li $t6, 5
	divu $v0, $t6
	mfhi $t5
	beqz $t5, true
	li $v1, 0
	j exitfe
	
	exitfe:
	jr $ra

decodeBacon:
	li $t3, 0 #decoded char
	li $t4, 0 #5 char counter
	li $t1, 0 #message length
	li $t2, 0 # 5 B counter
	addi $t5, $a3, -1
		
	loopdecodeBaconreal:
	beq $t4, 5, addChar
	lb $t0, 0($a0)
	addi $a0, $a0, 1
	beqz $t0, exitdecodeBacon
	beq $t1, $a3, exitdecodeBaconBuffer
	bne $t0, $a1, shiftleft
	addi $t2, $t2, 1
	addi $t3, $t3, 1
	sll $t3, $t3, 1
	addi $t4, $t4, 1
	j loopdecodeBaconreal
	
	
	shiftleft:
	sll $t3, $t3, 1
	addi $t4, $t4, 1
	j loopdecodeBaconreal
	
	addChar:
	srl $t3, $t3, 1
	beq $t2, 5, exitdecodeBaconEOM
	beq $t3, 26, addCharSpace
	beq $t3, 27, addCharExclamation
	beq $t3, 28, addCharApostrophe
	beq $t3, 29, addCharComma
	beq $t3, 30, addCharPeriod
	addi $t3, $t3, 65
	sb $t3, 0($a2)
	addi $a2, $a2, 1
	li $t4, 0
	addi $t1, $t1, 1
	li $t2, 0
	li $t3, 0
	j loopdecodeBaconreal
	
	addCharSpace:
	addi $t3, $t3, 6
	sb $t3, 0($a2)
	addi $a2, $a2, 1
	li $t4, 0
	addi $t1, $t1, 1
	li $t2, 0
	li $t3, 0
	j loopdecodeBaconreal
	
	addCharExclamation:
	addi $t3, $t3, 6
	sb $t3, 0($a2)
	addi $a2, $a2, 1
	li $t4, 0
	addi $t1, $t1, 1
	li $t2, 0
	li $t3, 0
	j loopdecodeBaconreal
	
	addCharApostrophe:
	addi $t3, $t3, 11
	sb $t3, 0($a2)
	addi $a2, $a2, 1
	li $t4, 0
	addi $t1, $t1, 1
	li $t2, 0
	li $t3, 0
	j loopdecodeBaconreal
	
	addCharComma:
	addi $t3, $t3, 15
	sb $t3, 0($a2)
	addi $a2, $a2, 1
	li $t4, 0
	addi $t1, $t1, 1
	li $t2, 0
	li $t3, 0
	j loopdecodeBaconreal
		
	addCharPeriod:
	addi $t3, $t3, 16 
	sb $t3, 0($a2)
	addi $a2, $a2, 1
	li $t4, 0
	addi $t1, $t1, 1
	li $t2, 0
	li $t3, 0
	j loopdecodeBaconreal
		
	exitdecodeBacon:
	sb $zero, 0($a2)
	addi $t1, $t1, 1
	move $v0, $t1
	li $v1, 0
	j exitdc
	
		
	exitdecodeBaconBuffer:
	move $v0, $a3
	sb $zero, 0($a2)
	li $v1, 0
	j exitdc
	
	exitdecodeBaconEOM:
	sb $zero, 0($a2)
	addi $t1, $t1, 1
	move $v0, $t1
	li $v1, 1
	j exitdc
	
	exitdc:
	jr $ra


.data
	#Define your memory here 
