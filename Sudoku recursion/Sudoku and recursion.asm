# Homework #4
# name: Mohamamd Khan
# sbuid: 109958150

.text
	.macro nl()
	la $a0, nl
	li $v0, 4
	syscall
	.end_macro
	
	.macro tab()
	la $a0, tab
	li $v0, 4
	syscall
	.end_macro
	
	.macro space()
	la $a0, space
	li $v0, 4
	syscall
	.end_macro

# Computes the Nth number of the Hofsadter Female Sequence
# public int F (int n)
#
F:
	addi $sp, $sp, -8 #make room on the stack
	sw $ra, 0($sp) #save ra
	sw $a0, 4($sp) #save n
	
	la $a0, fString #print out F: n
	li $v0, 4
	syscall
	tab()
	lw $a0, 4($sp)
	li $v0, 1
	syscall
	nl()
	lw $a0, 4($sp)
	
	beqz $a0, basecaseF #if n = 0 then you have reached base case
	
	addi $a0, $a0, -1 #calculate n-1 and put in t1
	jal F #F(n-1) and into v0
	move $a0, $v0 #setting up M
	jal M #M(F(n-1))
	lw $t2, 4($sp)
	sub $v0, $t2 , $v0
	j exitF #put everything back on stack
	
	
	basecaseF:
	li $v0, 1
	exitF:
	lw $a0, 4($sp)
	lw $ra, 0($sp)
	addi $sp, $sp, 8
	jr $ra

#
# Computes the Nth number of the Hofsadter Male Sequence
# public int M (int n)
#	
M:
	addi $sp, $sp, -8 #make room on the stack
	sw $ra, 0($sp) #save ra
	sw $a0, 4($sp) #save n
	
	la $a0, mString #print out F: n
	li $v0, 4
	syscall
	tab()
	lw $a0, 4($sp)
	li $v0, 1
	syscall
	nl()
	lw $a0, 4($sp)
	
	beqz $a0, basecaseM #if n = 0 then you have reached base case
	
	addi $a0, $a0, -1 #calculate n-1 and put in t1
	jal M #M(n-1) and into v0
	move $a0, $v0 #setting up F
	jal F #F(M(n-1))
	lw $t2, 4($sp)
	sub $v0, $t2 , $v0
	j exitM #put everything back on stack
	
	
	basecaseM:
	li $v0, 0
	exitM:
	lw $a0, 4($sp)
	lw $ra, 0($sp)
	addi $sp, $sp, 8
	jr $ra

#
# Tak Function
# public int tak (int x, int y, int z)
#
tak:
	addi $sp, $sp, -28
	sw $ra, 0($sp)
	sw $a0, 4($sp)
	sw $a1, 8($sp)
	sw $a2, 12($sp)
	
	bge $a1, $a0, baseTak
	
	addi $a0, $a0, -1 #a0 = x-1
	lw $a1, 8($sp) #a1 = y
	lw $a2, 12($sp) #a3
	jal tak #tak(x-1, y, z)
	sw $v0, 16($sp)
	
	lw $t1, 8($sp) #t1 = y
	addi $a0, $t1, -1 #a0 = y-1
	lw $a1, 12($sp) #a1 = z
	lw $a2, 4($sp) #a2 = x
	jal tak #tak(y-1, z, x)
	sw $v0, 20($sp)
	
	lw $t2, 12($sp)
	addi $a0, $t2, -1 #a0 = z-1
	lw $a1, 4($sp) #a1 = x
	lw $a2, 8($sp) #a2 = y
	jal tak #tak(z-1, x, y)
	sw $v0, 24($sp)
	
	lw $a0, 16($sp)
	lw $a1, 20($sp)
	lw $a2, 24($sp)
	jal tak #tak(tak(x-1, y, z), tak(y-1, z, x), tak(z-1, x, y))
	j exitTak
	
	
	baseTak:
	lw $v0, 12($sp)
	exitTak:
	lw $ra, 0($sp)
	lw $a0, 4($sp)
	lw $a1, 8($sp)
	lw $a2, 12($sp)
	addi $sp, $sp, 28
	jr $ra

#
# Helper function for solving sudoku
# public boolean isSolution (int row, int col)
#
isSolution:
	bne $a0, 8, isNotSolution
	beq $a1, 8, isSolutionTrue 
	
	isNotSolution:
	li $v0, 0
	jr $ra
	
	isSolutionTrue:
	li $v0, 1
	jr $ra

#
# Helper function for solving sudoku
# public void printSolution (byte[][] board)
#
printSolution:
	move $t0, $a0 #move address of array into t0
	li $t1, 0 #i
	li $t2, 0 #j
	li $t3, 9
	move $t5, $t0 #base address of array
	la $a0, solution
	li $v0, 4 
	syscall
	nl()
	
	loopPrintSolution:
	bgt $t1, 8, exitPrintSolution
	bgt $t2, 8, addOneToRow
	lb $t6, 0($t0)
	move $a0, $t6
	li $v0, 1
	syscall
	space()
	
	addi $t2, $t2, 1
	
	mul $s2, $t1, $t3 #i * cols
	add $s2, $s2, $t2 #i * cols +j
	add $t0, $s2, $t5 #base address + i * cols * 1 +j as the new address 

	
	j loopPrintSolution
	
	addOneToRow:
	addi $t1, $t1, 1
	li $t2, 0
	nl()
	j loopPrintSolution
	
	exitPrintSolution:
	jr $ra

#
# Helper function for solving sudoku
# public (byte [], int) gridSet (byte[][] board, int row, int col)
#
gridSet:
	li $t0, 3
	li $t3, 0
	la $t8, gSet #address of gSet
	li $t9, 0
	li $s4, 9
	div $a1, $t0
	mflo $t1 
	mul $t1, $t1, $t0
	mflo $t1 #r_start	
	
	div $a2, $t0
	mflo $t2 
	mul $t2, $t2, $t0
	mflo $t2 #c_start
	
	move $s2, $t1 #original r_start
	move $s3, $t2 #original c_start
	
	addi $t4, $t1, 3 #r_start + 3
	addi $t5, $t2, 3 #c_start + 3
	
	loopGridSet:
	bge $t1, $t4, returnGridSet
	bge $t2, $t5, incrementGridRow
	mul $s1, $t1, $s4 #i * # of cols
	add $s1, $s1 $t2 #i* cols + j
	add $t6, $s1, $a0 #base address + i * cols + j
	lb $t7, 0($t6) #load board[i][j]
	beqz $t7, incrementColGrid
	sb $t7, 0($t8) #store board[i][j] into gSet[count]
	addi $t9, $t9, 1 #increment number of numbers in the 3x3 grid
	addi $t8, $t8, 1 #increment the base address
	incrementColGrid:
	addi $t2, $t2, 1 #increment j
	j loopGridSet
	
	incrementGridRow:
	addi $t1, $t1, 1
	addi $t2, $t2, -3
	j loopGridSet
	
	returnGridSet:
	move $v0, $t9
	jr $ra

#
# Helper function for solving sudoku
# public (byte [], int) colSet (byte[][] board, int col)
#	
colSet:
	li $t0, 0 #counter of number of numbers
	li $t5, 0 #counter for loop
	move $t3, $a0 #base address of the array
	la $t4, cSet #address of rSet
	move $t6, $a1 #column of the array
	li $t8, 9 #amount of columns
	
	loopColSet:
	bgt $t5, 8, returnColSet
	mul $s1, $t5, $t8 #i * columns
	add $s1, $s1, $t6 #i * cols + j
	add $t2, $s1, $t3 #base address + i * cols +j as the new address 
	lb $t1, 0($t2)
	beqz $t1, incrementCol #blank = \0?
	sb $t1, 0($t4)
	addi $t0, $t0, 1
	addi $t4, $t4, 1
	incrementCol:
	addi $t5, $t5, 1
	j loopColSet
	
	returnColSet:
	move $v0, $t0
	jr $ra

#
# Helper function for solving sudoku
# public (byte [], int) rowSet (byte[][] board, int row)
#		
rowSet:
	li $t0, 0 #counter of number of numbers
	li $t5, 0 #counter for loop
	move $t3, $a0 #base address of the array
	la $t4, rSet #address of rSet
	move $t6, $a1 #row of the array
	li $t8, 9 #amount of columns
	
	loopRowSet:
	bgt $t5, 8, returnRowSet
	mul $s1, $t8, $t6 #i * columns
	add $s1, $s1, $t5 #i * cols + j
	add $t2, $s1, $t3 #base address + i * cols +j as the new address 
	lb $t1, 0($t2)
	beqz $t1, incrementRow #blank = \0?
	sb $t1, 0($t4)
	addi $t0, $t0, 1
	addi $t4, $t4, 1
	incrementRow:
	addi $t5, $t5, 1
	j loopRowSet
	
	returnRowSet:
	move $v0, $t0
	
	jr $ra

#
# Helper function for solving sudoku
# public (byte [], int) colSet (byte[][] board, int row, int col)
#			
constructCandidates:
	li $t0, 0 #count variable
	li $t4, 1 #main loop counter
	li $t5, 0 #inner loop counter
	move $t7, $a3 #base address of candidates array
	la $s0, rSet
	la $s1, cSet
	la $s2, gSet
	addi $sp $sp, -60
	sw $a0, 0($sp) #byte[][] board
	sw $a1, 4($sp) #int row
	sw $a2, 8($sp) #int col
	sw $a3, 12($sp) #byte[] candidates
	sw $ra, 16($sp)
	sw $s0, 32($sp)
	sw $s1, 36($sp)
	sw $s2, 40($sp)
	sw $t0, 44($sp)
	sw $t4, 48($sp)
	sw $t5, 52($sp)
	sw $t7, 56($sp)
	
	jal rowSet
	sw $v0, 20($sp)  #rLength
	
	lw $a1, 8($sp) 
	jal colSet
	sw $v0, 24($sp) #cLength
	
	lw $a1, 4($sp)
	lw $a2, 8($sp)
	jal gridSet
	sw $v0, 28($sp) #gLength	
	
	
	 
	lw $a0, 0($sp) #byte[][] board
	lw $a1, 4($sp) #int row
	lw $a2, 8($sp) #int col
	lw $a3, 12($sp) #byte[] candidates
	lw $ra, 16($sp)
	lw $s0, 32($sp)
	lw $s1, 36($sp)
	lw $s2, 40($sp)
	lw $t0, 44($sp)
	lw $t4, 48($sp)
	lw $t5, 52($sp)
	lw $t7, 56($sp)   
	lw $t1, 20($sp)
	lw $t2, 24($sp)
	lw $t3, 28($sp)

	
	loopConstructCandidates:
	bgt $t4, 9, returnConstructCandidates
	
	lw $s0, 32($sp)
	loopRSet:
	bge $t5, $t1, resetInnerCounterC
	lb $t6, 0($s0)
	beq $t6, $t4, incrementMainCounter
	addi $t5, $t5, 1
	addi $s0, $s0, 1
	j loopRSet
	
	
	loopCSet:
	bge $t5, $t2, resetInnerCounterG
	lb $t6, 0($s1)
	beq $t6, $t4, incrementMainCounter
	addi $t5, $t5, 1
	addi $s1, $s1, 1
	j loopCSet 
	
	
	loopGSet:
	bge $t5, $t3, addToCandidates
	lb $t6, 0($s2)
	beq $t6, $t4, incrementMainCounter
	addi $t5, $t5, 1
	addi $s2, $s2, 1
	j loopGSet
	  
	resetInnerCounterC:
	li $t5, 0
	lw $s1, 36($sp)
	j loopCSet
	 
	resetInnerCounterG:
	lw $s2, 40($sp)
	li $t5, 0
	j loopGSet
	 
	incrementMainCounter:
	addi $t4, $t4, 1
	j loopConstructCandidates
	 
	addToCandidates:
	sb $t4, 0($t7)
	addi $t0, $t0, 1 #count++
	addi $t4, $t4, 1 #i++
	addi $t7, $t7, 1 #next byte in the candidate array
	j loopConstructCandidates
	  
	returnConstructCandidates:
	move $v0, $t0
	addi $sp, $sp, 60
	jr $ra

#
# sudoku solver function
# public (byte [], int) colSet (byte[][] board, int x, int y)
#	
sudoku:
	addi $sp, $sp -16
	sw $a0, 0($sp)#board
	sw $a1, 4($sp)#x
	sw $a2, 8($sp)#y
	sw $ra 12($sp)
	
	
	#if(isSolution(x,y))
	move $a0, $a1
	move $a1, $a2
	jal isSolution
	beq $v0, 1, solvedSudoku
	
	#else
	lw $t0, 4($sp) #x
	lw $t1, 8($sp) #y
	addi $t1, $t1, 1 #y = y + 1
	bgt $t1, 8, nextRow 
	
	checkCurrentSquare:
	#base addr + i* cols + j
	li $t2, 9
	lw $t3, 0($sp) #base address
	mul $s0, $t0, $t2 #i * 9
	add $s0, $s0, $t1 #i * 9 + j
	add $s0, $s0, $t3, #base address + i*9 + j
	lb $t4, 0($s0) #board[x][y]
	beqz $t4, constructChoices #if board[x][y] == blank then find all possible choices
	move $a0, $t3
	move $a1, $t0
	move $a2, $t1
	jal sudoku
	
	constructChoices:
	addi $fp, $fp, -16
	move $a0, $t3 #board
	move $a1, $t0 #x
	move $a2, $t1 #y
	lw $a3, 0($fp) #candidates
	sw $t0, 16($sp)
	sw $t1, 20($sp)
	sw $t3, 24($sp)
	jal constructCandidates
	move $t5, $v0 #candidateLength
	li $t6, 0 #candidateLengthLoop counter
	candidateLengthLoop:
	bge $t6, $t5, exitSudoku
	lw $t0, 16($sp) #x
	lw $t1, 20($sp) #y
	li $t2, 9
	mul $s0, $t0, $t2 #i * 9
	add $s0, $s0, $t1 #i * 9 + j
	add $s0, $s0, $t3, #base address + i*9 + j
	
	
	
	solvedSudoku:
	lw $a0, 0($sp)
	jal printSolution
	j exitSudoku
	
	nextRow:
	addi $t0, $t0, 1 #x = x + 1
	li $t0, 0
	j checkCurrentSquare
	
	exitSudoku:
	jr $ra


.data
tab: .asciiz "\t"
nl: .asciiz "\n"
fString: .asciiz "F: "
mString: .asciiz "M: "
solution: .asciiz "Solution: "
space: .asciiz " "
rSet: 		.byte 0:9
cSet: 		.byte 0:9
gSet: 		.byte 0:9
FINISHED: 	.byte 0
