.text

	.macro nlmain()
	la $a0, nlmain
	li $v0, 4
	syscall
	.end_macro
	
	.macro tabmain()
	la $a0, tabmain
	li $v0, 4
	syscall
	.end_macro

la $a0, board
li $a1, 3
li $a2, 3
la $a3, candidates
jal constructCandidates
move $a0, $v0
li $v0, 1
syscall

li $v0, 10
syscall

.data
tabmain: .asciiz "\t"
nlmain: .asciiz "\n"
board: .byte 9, 0, 6, 0, 7, 0, 4, 0, 3,
	   0, 0, 0, 4, 0, 0, 2, 0, 0,
	   0, 7, 0, 0, 2, 3, 0, 1, 0, 
	   5, 0, 0, 0, 0, 0, 1, 0, 0, 
	   0, 4, 0, 2, 0, 8, 0, 6, 0, 
	   0, 0, 3, 0, 0, 0, 0, 0, 5, 
	   0, 3, 0, 7, 0, 0, 0, 5, 0, 
	   0, 0, 7, 0, 0, 5, 0, 0, 0, 
	   4, 0, 5, 0, 1, 0, 7, 0, 8
candidates: .byte 0:9
.include "hw4.asm"
